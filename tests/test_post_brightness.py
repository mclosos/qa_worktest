import json
from random import uniform

import pytest
import pytest_check as check

from tests.support.requests import post, get
from tests.support.schema_validation import assert_post_response


def test_post_brightness_positive(connect_device):
    number = uniform(0, 10)
    response = post("brightness", data={"brightness": number})
    assert_post_response(response)

    data = json.loads(response.content)
    check.is_true(data["success"])
    state = json.loads(get("state").content)
    check.equal(state["brightness"], number)


@pytest.mark.skip("Bug #003")
@pytest.mark.parametrize("brightness", [-56.455, "test", None])
def test_post_brightness_negative(connect_device, brightness):
    brightness_before = json.loads(get("state").content)["brightness"]
    response = post("brightness", data={"brightness": brightness})
    check.is_false(json.loads(response.content)["success"])
    brightness_after = json.loads(get("state").content)["brightness"]
    check.equal(brightness_before, brightness_after)
