import json
import random

import pytest
import pytest_check as check

from tests.support.requests import post, get
from tests.support.schema_validation import assert_post_response


def test_post_color_positive(connect_device):
    r = lambda: random.randint(0, 255)
    color = ('#{}{}{}'.format(r(), r(), r()))
    response = post("color", data={"color": color})
    assert_post_response(response)

    data = json.loads(response.content)
    check.is_true(data["success"])
    state = json.loads(get("state").content)
    check.equal(state["color"], color)


@pytest.mark.skip("Bug #004")
@pytest.mark.parametrize("color", ["test", 500, None])
def test_post_color_negative(connect_device, color):
    color_before = json.loads(get("state").content)["color"]
    response = post("color", data={"color": color})
    check.is_false(json.loads(response.content)["success"])
    color_after = json.loads(get("state").content)["color"]
    check.equal(color_before, color_after)
