import json

from pytest_check import check
from requests import Response


def assert_get_devices_response(response: Response):
    devices = json.loads(response.content)
    for device in devices:
        with check:
            assert "name" in device
        with check:
            assert isinstance(device["name"], str)

        with check:
            assert "ip" in device
        with check:
            assert isinstance(device["ip"], str)


def assert_post_response(response: Response):
    response = json.loads(response.content)
    with check:
        assert "success" in response
    with check:
        assert isinstance(response["success"], bool)


def assert_get_state_response(response: Response):
    response = json.loads(response.content)
    with check:
        assert "name" in response
    with check:
        assert isinstance(response["name"], str)

    with check:
        assert "ip" in response
    with check:
        assert isinstance(response["ip"], str)

    with check:
        assert "color" in response
    with check:
        assert isinstance(response["color"], str)

    with check:
        assert "brightness" in response
    with check:
        assert isinstance(response["brightness"], float)
