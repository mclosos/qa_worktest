import pytest_check as check

from tests.support.requests import get
from tests.support.schema_validation import assert_get_devices_response


def test_get_devices_positive():
    response = get("devices")
    check.equal(response.status_code, 200)
    assert_get_devices_response(response)
