import pytest

from app import app, run
from tests.config.config import test_devices_ip_list
from tests.support.requests import post


@pytest.fixture()
def connect_device():
    post("disconnect")
    post("connect", {"ip": test_devices_ip_list[0]})
    yield
    post("disconnect")


@pytest.fixture()
def disconnect_device():
    post("disconnect")
    yield
    post("disconnect")
