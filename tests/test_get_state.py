import json

import pytest_check as check

from tests.support.requests import get
from tests.support.schema_validation import assert_get_state_response


def test_get_state_positive(connect_device):
    response = get("state")
    assert_get_state_response(response)


def test_get_state_negative(disconnect_device):
    data = json.loads(get("state").content)
    check.is_false(data["success"])
