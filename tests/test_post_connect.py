import json

import pytest_check as check

from tests.config.config import test_devices_ip_list
from tests.support.requests import post
from tests.support.schema_validation import assert_post_response


def test_post_connect_positive(disconnect_device):
    data = dict(ip=test_devices_ip_list[0])
    response = post("connect", data=data)
    check.equal(response.status_code, 200)
    assert_post_response(response)

    data = json.loads(response.content)
    check.is_true(data["success"])


def test_post_connect_second_device(disconnect_device):
    first_device = dict(ip=test_devices_ip_list[0])
    second_device = dict(ip=test_devices_ip_list[1])
    first_device_connect = post("connect", first_device)
    check.equal(first_device_connect.status_code, 200)

    second_device_connect = post("connect", second_device)
    second_device_connect_data = json.loads(second_device_connect.content)
    check.is_false(second_device_connect_data["success"])

    post("disconnect")
    second_device_connect_retry = post("connect", second_device)
    second_device_connect_retry_data = json.loads(second_device_connect_retry.content)
    check.is_true(second_device_connect_retry_data["success"])
