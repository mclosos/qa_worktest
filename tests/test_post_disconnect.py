import json

import pytest_check as check

from tests.support.requests import post
from tests.support.schema_validation import assert_post_response


def test_post_disconnect_connected(connect_device):
    response = post("disconnect")
    assert_post_response(response)

    data = json.loads(response.content)
    check.is_true(data["success"])
