import json
import random
import string

import pytest
import pytest_check as check

from tests.support.requests import post, get
from tests.support.schema_validation import assert_post_response


def test_post_name_positive(connect_device):
    characters = string.ascii_letters + string.digits + string.punctuation
    name = (''.join(random.choice(characters) for _ in range(100)))
    response = post("name", data={"name": name})
    assert_post_response(response)

    data = json.loads(response.content)
    check.is_true(data["success"])
    state = json.loads(get("state").content)
    check.equal(state["name"], name)


@pytest.mark.skip("Bug #005")
@pytest.mark.parametrize("name", [None, {"foo": "bar"}])
def test_post_color_negative(connect_device, name):
    name_before = json.loads(get("state").content)["name"]
    response = post("name", data={"name": name})
    check.is_false(json.loads(response.content)["success"])
    name_after = json.loads(get("state").content)["name"]
    check.equal(name_before, name_after)
