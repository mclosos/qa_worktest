## Reports

1. [Bugs](#bugs)
    1. [#001](#001)
    2. [#002](#002)
    3. [#003](#003)
    4. [#004](#004)
    5. [#005](#005)
2. [Suggestions](#suggestions)

## Bugs

### #001
#### Wrong key for POST /connect body causes 500

`curl localhost:8080/connect -X POST -d '{"foo":"bar"}' -H "Content-type:application/json"`

#### Actual result:

>Error: 500 Internal Server Error

#### Expected result 

> 422 or 400 response code

---

### #002
####  Json body and header for POST /disconnect causes 500

`curl -X POST localhost:8080/disconnect -d '{"string":"string"}' -H "Content-type:application/json"`

#### Actual result:

>Error: 500 Internal Server Error

#### Expected result 

> 422 or 400 response code

---

### #003
#### Non float brightness value for POST /brightness causes 500


`curl localhost:8080/brightness -X POST -d '{"brightness":"test"}' -H "Content-type:application/json"`

#### Actual result:

>Error: 500 Internal Server Error
>ValueError(&quot;could not convert string to float: &#039;test&#039;&quot;)

#### Expected result 

> 422 or 400 response code

---

### #004
#### Non string color for POST /color causes 500

`curl localhost:8080/color -X POST -d '{"color":500.0}' -H "Content-type:application/json"`

#### Actual result:
>Error: 500 Internal Server Error
>AttributeError(&quot;&#039;float&#039; object has no attribute &#039;lstrip&#039;&quot;)

#### Expected result 

> 422 or 400 response code

---

### #005
#### Non string types are allowed for name value in POST /name

`curl localhost:8080/name -X POST -d '{"name":{"foo": "bar"}}' -H "Content-type:application/json"`

#### Actual result:

> 200 response code

> {"success": true}

> {"name": {"foo": "bar"}, "ip": "192.168.100.10", "color": "#ffffff", "brightness": 10.0}

#### Expected result 

> 422 or 400 response code

---

## Suggestions

#### Response headers

Use content type application/json for response headers. 

#### Success false

It is strange that we return code 200 and {success: false} in body. Do you see a contradiction here? :) 
