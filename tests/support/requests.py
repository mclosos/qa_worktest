import requests
from requests import Response

from tests.config.config import host


def post(route: str, data: dict = None) -> Response:
    if data is None:
        data = {}
    return requests.post("{}/{}".format(host, route), json=data)


def get(route: str) -> Response:
    return requests.get("{}/{}".format(host, route))
